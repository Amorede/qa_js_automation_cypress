/// <reference types="Cypress" />

context('OOS', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')

    });
  
    // https://on.cypress.io/interacting-with-elements
  
    it('Contains Name and City fields ', () => {
        cy.get('#filters');
        cy.contains('Name');
        cy.contains('City');
    });

    it('to find by name lloyd', () => {
        cy.get('#name').click().type('lloyd');
        cy.get('button[type=submit]').click();
        cy.get('div[class=CrewMemeber-name]').contains('lloyd');
      });

    it('to find by city', () => {
        cy.get('#city').click().type('worcester');
        cy.get('button[type=submit]').click();
        cy.get('div[class=CrewMemeber-name]').contains('worcester');
      });
 
})