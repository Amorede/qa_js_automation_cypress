/// <reference types="Cypress" />

context('Actions', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')

    });
  
    // https://on.cypress.io/interacting-with-elements
  
    it('Contains Name and City fields ', () => {
      cy.get('#filters');
      cy.contains('Name');
      cy.contains('City');
    });

    it('to find lloyd', () => {
        cy.get('#name').click().type('lloyd');
        cy.get('button[type=submit]').click();
        cy.get('div[class=CrewMemeber-name]').contains('lloyd');
      });
 
    //     // .type() with special character sequences
    //     .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
    //     .type('{del}{selectall}{backspace}')
  
    //     // .type() with key modifiers
    //     .type('{alt}{option}') //these are equivalent
    //     .type('{ctrl}{control}') //these are equivalent
    //     .type('{meta}{command}{cmd}') //these are equivalent
    //     .type('{shift}')
  
    //     // Delay each keypress by 0.1 sec
    //     .type('slow.typing@email.com', { delay: 100 })
    //     .should('have.value', 'slow.typing@email.com')
  
    //   cy.get('.action-disabled')
    //     // Ignore error checking prior to type
    //     // like whether the input is visible or disabled
    //     .type('disabled error checking', { force: true })
    //     .should('have.value', 'disabled error checking')
    // })
})